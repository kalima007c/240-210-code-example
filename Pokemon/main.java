class main{
    public static void main(String[] args) {

        Pokemon P1 = new Pokemon("Pikachu");
        Pokemon P2 = new Pokemon("Raichu");
        Pokemon P3 = new Pokemon("Niaz");
        
        PokemonCenter pc = new PokemonCenter();
        pc.addPoke(P1);
        pc.addPoke(P2);
        pc.addPoke(P3);
        
        System.out.println("BEFORE FEED");
        pc.printPokemon();

        System.out.println("AFTER FEED");
        pc.feed();
        pc.printPokemon();
    }
}