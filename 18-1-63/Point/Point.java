class Point {
    private double x,y,z;

    Point(){}
    Point(double _X,double _Y,double _Z) {
        this.x = _X;
        this.y = _Y;
        this.z = _Z;
    }
    // Mutator
    public void setPoint(Point P) {
        this.x = P.getX();
        this.y = P.getY();
        this.z = P.getZ();
    }
    public void setX(double _X) {
        this.x = _X;
    }
    public void setY(double _Y) {
        this.y = _Y;
    }
    public void setZ(double _Z) {
        this.z = _Z;
    }

    // Accessor

    public double getX() {
        return this.x;
    }
    public double getY() {
        return this.y;
    }
    public double getZ() {
        return this.z;
    }
    public void showDetail(){
        System.out.println("X= "+this.x);
        System.out.println("Y= "+this.y);
        System.out.println("Z= "+this.z);
        System.out.println("\n");
    }
	

}