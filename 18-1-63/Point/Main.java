class Main {

    public static void main(String args[]) {
        Point p1 , p2 , p3;
        
            p1 = new Point();
            p2 = new Point(1.0,2.0,3.0);
            p3 = new Point();

            p1.setPoint(p2);
            
            p3.setX(-5.0);
            p3.setY(6.0);
            p3.setZ(9.5);

            System.out.println("Print from P1");
            p1.showDetail();

            System.out.println("Print from P2");
            p2.showDetail();
            System.out.println("Print from P3");

            System.out.println("X = "+p3.getX()+" Y = "+p3.getY()+" Z = "+p3.getZ());
            
    }   
}

