class Character {
    private boolean isAlive = true;
    private String char_name;
    private int MaxHp;
    private int MaxMana;
    private int hp;
    protected int mana;
    private int atk;

    Character(String name,int MaxHp,int MaxMana){
        this.char_name = name;
        this.MaxHp = hp = MaxHp;
        this.MaxMana= mana = MaxMana;
        this.atk = 10;
    }
    public void setAtk(int atk){
        this.atk = atk;
    }
    public void attack(Monster Target) {
        Target.onAttack(this.atk);
    }
    public void onAttack(int attack){
        this.hp -= attack;
        if(this.hp <= 0 ){
            isAlive = false;
        }
    }

    public void printStatus() {
        System.out.println(" NAME : "+this.char_name);
        System.out.println(" HP : "+this.hp+"/"+this.MaxHp);
        System.out.println(" Mana : "+this.mana+"/"+this.MaxMana);
        System.out.println(" Attack : "+this.atk);
        System.out.println(" Alive : "+this.isAlive);
    } 
}