class Magician extends Character {
    Magician(String name,int maxhp,int maxmp)
    {
        super(name,maxhp,maxmp);
        this.setAtk(20);
    }
    public void firebolt(Monster Target){
        this.mana -= 10;
        Target.onAttack(30);
        
    }
    public void lightning_bolt(Monster Target){
        this.mana -= 25;
        Target.onAttack(50);
    }
}