import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class App extends JFrame {
    private JPanel      JP1      = new JPanel(),
                        JP2      = new JPanel();                        // Create new JPanel
    private JButton     JBT_1    = new JButton("Increase");             // Create JButton Instance
    private JLabel      _label   = new JLabel("Count : "),
                        f_label  = new JLabel("Farenheit : "),
                        c_label  = new JLabel("Celcius : ");            // Create JLabel Instance
    private JTextField  count_tf = new JTextField("0",20),              
                        f_tf     = new JTextField("0",20),
                        c_tf     = new JTextField("0",20);              // Create JTextField Instance
    App(){
        setSize(600,200);                                               // Set Display Size
        setLayout(new GridLayout(2,1));                                 // Set JFrame's Layout
        setTitle("GUI Component");                                      // Set name of titlebar

        JP1.setLayout(new FlowLayout());                                // Set layout of JP1 panel
            JP1.add(_label);                                            // Line 21 - 23 add object into JP1 Panel 
            JP1.add(count_tf);
            JP1.add(JBT_1);                                         
        JP2.setLayout(new GridLayout(2,2));                             // Set layout of JP2 panel
            JP2.add(f_label);                                           // Line 25 - 28 add object into JP2 Panel 
            JP2.add(f_tf);
            JP2.add(c_label);
            JP2.add(c_tf);
        add(JP1);                                                       // Add JP1 Panel to JFrame
        add(JP2);                                                       // Add JP2 Panel to JFrame

        setVisible(true);                                               // Set JFrame to Visible
        setDefaultCloseOperation(EXIT_ON_CLOSE);                        // Terminated Program when you click close button on right side of window
    }
}

class Main {
    public static void main(String[] args) {
        new App();                                                      // Create App Instance and run it
    }
}
