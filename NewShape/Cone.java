class Cone extends Shape {
    private double radius;
    private double height;

    Cone(String _name,double r,double h){
        super(_name);
        radius = r;
        height = h;
    }

    public double getVolume(){
        double v = 1.0/3 * Math.PI * radius * radius * height;
        return v;
    }
}